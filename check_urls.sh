#!/bin/bash

########################################################################
###### Ce script verifie l'etat des sites web listés dans url_list.txt
###### en recuperant la page d'index et leur statut par curl
###### les pages d'index permettent de produire des checksums
###### qui sont comparees a une version historique sha256sum.txt.prev
######
######
######
###### En cas de rajout de nouveau site dans le fichier url_list.txt
###### il faut donc de nouveau lancer le checks.sh avec l'option
###### "init"
######
###### Un lancement normal se fait avec l'option "check" et produit
###### des fichiers log et json dans un sous-dossier workdir où
###### se fait tout le travail.
###### 
###### En cas de site dynamique, la comparaison des checksums par
###### rapport a la version originelle produira une erreur.
###### Il faudra alors lancer le script avec l'option "compare"
###### qui va faire des diff entre les fichiers html
###### pour permettre d'identifier les elements changeant dans la page.
######
###### Si la solution utilisée produit une page dynamique et qu'elle 
###### n'est pas connue du script il faudra la rajouter. Pour ça, voir
###### la fonction remove_dyn_elems
########################################################################

# En cas d'update des fichiers wordpress avec InfiniteWP, il faudra
# regenerer tous les checksums


#set -x

CURDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DATE=`date '+%Y%m%d_%H%M%S'`

# path to commands; could be useful for crons
CURL=`command -v curl`
WGET=`command -v wget`
SHA256SUM=`command -v sha256sum`
AWK=`command -v awk`
DIFF=`command -v diff`


usage="$0 [compare|init|check|clean|--help]\n\n
    \tcompare: display more informations on differences since previous run\n
    \tcompare2init: display more informations on differences since first run\n
    \tinit: perform a check an compute the sha256sum.txt.prev file and a status.log file (see bellow)\n
    \tcheck: run a basic check - get http answer and compare index checksums to the first run\n
    \tclean: remove the workdir\n
    \t--help: print this help and exit.\n
\n\n
    If you run this code with the check option, it will compare the checksums to the previous one and build the status.log file built from the server HTTP answers.\n
    Finally, it will export those results to a checksum.json file and a status.json file.\n
\n\n
    If you add a new website to url_list.txt, use the init option
\n\n
    If you want to show differences about what have changed, use the compare option.
    "


function remove_dyn_elems {

    # listing of dynamic elements, depending on product solution
    wp_regexp_arr=('search-form' 'cropped' 's?ver=' '.min.css?ver=')
    wp_regexp_plugin_wedevsprojectmanager="pusher.+ver="
    wp_regexp_plugin_gravityform='ajax.php'
    wp_regexp_plugin_calendar="\sid=\"today\""
    wp_regexp_plugin_rsswidget="rsswidget"
    wp_regexp_plugin_sfsi="sfsi_plugin_version"
    gitlab_regexp_arr=("authenticity_token" "csrf-token" src\\\=\\\"\\\/assets\\\/webpack\\\/.+\.[A-Za-z0-9]{8}\.\(chunk\|bundle\).js\\\")
    typesetter_regexp_arr=("wgBackendResponseTime" "wgRequestId" "time" "[0-9]{1,3}(\.[0-9]{1,2})*%\s+[0-9]{1,2}\.[0-9]{3}")
    nextcloud_regexp_arr=("token" "nonce" "core\/css\/guest.css" "initial-state-theming-data" "initial-state-core-config")
    nextcloud_regexp_plugin_deck='deck/css/activity.css?v='
    gitbook_regexp_arr=("data-revision" "mtime")
    hesk_rule='165d'
    shinyproxy_regexp_arr=('_csrf')
    rstudio_regexp_arr=('csrf-token')

    while read name website type product plugins activity owner
    do
        if [[ "${name}" =~ [[:space:]]*# ]] || [[ "${name}" == "" ]] || [[ "${activity}" == "inactive" ]]; then
            continue
        else
            if [[ "${type}" == "dyn" ]]; then
                case ${product} in
                    wp|wordpress )
                        for wp_regexp in ${wp_regexp_arr[@]}
                        do
                            sed -i -e /$wp_regexp/d $name.index.html
                        done
                        plugins_nl=$(echo $plugins | tr "," "\n")
                        for plugin in $plugins_nl
                        do
                            case $plugin in
                                "wedevs-project-manager" )
                                    sed -ri -e /$wp_regexp_plugin_wedevsprojectmanager/d $name.index.html
                                ;;
                                "rsswidget" )
                                    sed -ri -e /$wp_regexp_plugin_rsswidget/d $name.index.html
                                ;;
                                "gravityform" )
                                    sed -ri -e /$wp_regexp_plugin_gravityform/d $name.index.html
                                ;;
                                "calendar" )
                                    sed -ri -e /$wp_regexp_plugin_calendar/d $name.index.html
                                ;;
                                "sfsi" )
                                    sed -i -e /$wp_regexp_plugin_sfsi/d $name.index.html
                                ;;
                                * )
                                ;;
                            esac
                        done
                    ;;
                    "typesetter" )
                        for typesetter_regexp in ${typesetter_regexp_arr[@]}
                        do
                            sed -ri -e /$typesetter_regexp/d $name.index.html
                        done
                    ;;
                    "shinyproxy" )
                        for shinyproxy_regexp in ${shinyproxy_regexp_arr[@]}
                        do
                            sed -ri -e /$shinyproxy_regexp/d $name.index.html
                        done
                    ;;
                    "rstudio" )
                        for rstudio_regexp in ${rstudio_regexp_arr[@]}
                        do
                            sed -ri -e /$rstudio_regexp/d $name.index.html
                        done
                    ;;
                    "gitlab" )
                        for gitlab_regexp in ${gitlab_regexp_arr[@]}
                        do
                            sed -ri -e /$gitlab_regexp/d $name.index.html
                        done
                    ;;
                    "gitbook" )
                        for gitbook_regexp in ${gitbook_regexp_arr[@]}
                        do
                            sed -ri -e /$gitbook_regexp/d $name.index.html
                        done
                    ;;
                    "hesk" )
                        sed -i -e ${hesk_rule} $name.index.html
                    ;;
                    "nextcloud" )
                        for nextcloud_regexp in ${nextcloud_regexp_arr[@]}
                        do
                            sed -ri -e /$nextcloud_regexp/d $name.index.html
                        done
                        plugins_nl=$(echo $plugins | tr "," "\n")
                        for plugin in $plugins_nl
                        do
                            case $plugin in
                                "deck" )
                                    sed -i -e /$nextcloud_regexp_plugin_deck/d $name.index.html
                                ;;
                                * )
                                ;;
                            esac
                        done
                    ;;
                    * )
                    ;;
                esac
            fi
        fi
    done < $1
}

if [ -f /etc/default/websitechecks ]; then
  . /etc/default//websitechecks
  if [ -z ${WORKDIR} ]; then
    WORKDIR=${VARDIR}/workdir
  fi
  if [ -f "${ETCDIR}/url_list.txt" ]; then
    URL_LIST="${ETCDIR}/url_list.txt"
  fi
  if [ -f "${LIBEXECDIR}/check_sha256.awk" ]; then
    CHECK_SHA256="${LIBEXECDIR}/check_sha256.awk"
  fi
else
  . ./profile.conf
  cd ${DIR}
  if [ -z ${WORKDIR} ]; then
    WORKDIR=${DIR}/workdir
  fi
  URL_LIST="../url_list"
  CHECK_SHA256="../check_sha256.awk"
fi

if ${CHECK_CERT}; then
    CURL_OPTS="-Is"
else
    CURL_OPTS="-Iks"
fi

if [ -z "${OUTPUT_DIR}" ]; then
    OUTPUT_DIR="${DIR}/results"
fi
if [ ! -d ${OUTPUT_DIR} ]; then
    mkdir -p ${OUTPUT_DIR};
fi
OUTPUT_CHECKSUMS="${OUTPUT_DIR}/${DATE}_checksums.json"
OUTPUT_STATUS="${OUTPUT_DIR}/${DATE}_status.json"

if [[ "$1" == "compare" ]]; then
    cd ${WORKDIR}
    ext2compare="prev"
    COMPARE=true
elif [[ "$1" == "compare2init" ]]; then
    cd ${WORKDIR}
    COMPARE=true
    ext2compare="init"
elif [[ "$1" == "init" ]]; then
    rm -rf ${WORKDIR}
    CHECK=true
    FIRST=true
elif [[ "$1" == "clean" ]];then
    rm -rf ${WORKDIR}
    exit 0
else
    CHECK=true
    if [[ "$1" != "check" ]]; then
        echo -e $usage
        exit 0
    fi
fi


if [ ! -d ${WORKDIR} ]; then
    mkdir ${WORKDIR}
fi
cd ${WORKDIR}

if [ ${CHECK} ]; then
  rm status.log *.html sha256sum.txt 2>/dev/null
  while read name website type product plugins activity owner
  do
    if [[ "$name" =~ [[:space:]]*# ]] || [[ "$name" == "" ]] || [[ "$activity" == "inactive" ]]; then
      continue
    else
      echo -n $website" " >> status.log && $CURL $CURL_OPTS $website|head -1 >> status.log
      $WGET -O "${name}.index.html" $website 2>/dev/null
    fi
  done < ${URL_LIST}
  /usr/bin/dos2unix status.log 2>/dev/null
  remove_dyn_elems ${URL_LIST}
fi

if [ -z ${FIRST} ]; then
    $SHA256SUM *.html > sha256sum.txt
    
    # to check from the file
    # sha256sum -c sha256sum.txt
    # or the previous one
    # sha256sum -c sha256sum.txt.prev
    
    if [ ${COMPARE} ]; then
        ${DIFF} sha256sum.txt sha256sum.txt.prev |tee modified_websites.txt
        websites_modified=`awk '/index.html/ {print $3}' modified_websites.txt|uniq`
        for website_modified in $websites_modified
        do
            prev_file=`echo "${website_modified}.${ext2compare}"`
            echo ""
            echo "#################################################################"
            echo "## Checking if there is any file to compare with new result... ##"
            echo "#################################################################"            
            echo ""
            if [ -f $website_modified ] && [ -f $prev_file ]; then
                echo $DIFF -Ebw $prev_file $website_modified
                echo ""
                $DIFF $prev_file $website_modified
            fi
        done
        #rm modified_websites.txt
    else
        $DIFF -q sha256sum.txt sha256sum.txt.prev 1>/dev/null 2>&1
        if [ $? -eq 1 ]; then
            $DIFF sha256sum.txt sha256sum.txt.prev|$AWK '/[az]/ {print $3}' |uniq
        fi
    fi
    
    total=`cat sha256sum.txt*|wc -l`
    rm checksums.json 2>/dev/null
    echo "{" > checksums.json
    while read name url type product plugins activity owner
    do 
        if [[ "$name" =~ [[:space:]]*# ]] || [[ "$name" == "" ]] || [[ "$activity" == "inactive" ]]; then
            continue
        else
            $AWK -f ${CHECK_SHA256} -v url=$url -v sitename=${name} -v total=$total sha256sum.txt*
        fi
    done < ${URL_LIST} |sort >> checksums.json
    echo "}" >> checksums.json
else
    $SHA256SUM *.html > sha256sum.txt.prev
    for file in `ls *.html`; do 
        cp $file $file.init
    done
fi

if [ ${CHECK} ]; then
  total=`cat status.log|wc -l`
  rm status.json 2>/dev/null
  echo "{" > status.json
  $AWK -v total=$total '{ if ($NF=="200") {$NF="OK";} if (NR==total) {print "\t\""$1"\": \""$NF"\"";} else {print "\t\""$1"\": \""$NF"\",";} }' status.log >> status.json
  echo "}" >> status.json

  for file in `ls *.html`; do 
    cp $file $file.prev
  done

  # change current directory to $DIR b/c of issues with relative paths in profile.conf
  cd ${DIR}
  cp ${WORKDIR}/status.json ${OUTPUT_STATUS}
  if [ -f ${WORKDIR}/checksums.json ]; then
    cp ${WORKDIR}/checksums.json ${OUTPUT_CHECKSUMS}
  fi
fi

cd ${CURDIR}
