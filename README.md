# website\_checks

This set of scripts allow some basic tests to check websites :
  - https certificates status,
  - checksums of the index web pages,
  - HTTP answers.

`ssl-cert-info.sh` is a modified version from https://gist.github.com/stevenringo/2fe5000d8091f800aee4bb5ed1e800a6 already taken from http://giantdorks.org/alain/shell-script-to-check-ssl-certificate-info-like-expiration-date-and-subject/

## How to use

2 main scripts are used here : `check_certs.sh` and `check_urls.sh`.

Each script is looking at a txt file to scan these hosts / urls. Each line is something to check.

First, you will need to enter some informations in `profile.conf` file. This file is sourced by those 2 scripts.

Then, edit `host_https_list.txt` and `url_list.txt` to fit your needs.

## How to install

### Requirements

Requirements are really basics linux softwares:

  - gnu make,
  - awk & sed,
  - sudo (only needed for a system wide install),
  - curl & wget,
  - sha256sum to perform checksums

___

After editing the 3 configuration files (`profile.conf` and listings `host_https_list.txt`, `url_list.txt`), just run:

```bash
sudo make install
```

Once `websitechecks` is installed, you should find those files here:
  
  - `/etc/default/websitechecks`
  - `/usr/local/websitechecks/etc/host_https_list.txt`
  - `/usr/local/websitechecks/etc/url_list.txt`

## How to check your HTTPS certificates

Edit `host_https_list.txt` file containing the list of hosts (fqdn / fully qualified domain name) to scan.

Then, just run:

```bash
check_certs.sh
```

This will produce a json output on the standard output and in the output directory (`OUTPUT_DIR` defined in `profile.conf` (default is `/usr/local/websitechecks/results`)).

## How to use checks.sh

This script checks some URLs (HTTP answers + checksums). Checksums are a good way to check if a website has been defaced. Indeed, any modification on a webpage could e legitiate or not, and this should be monitored.
Note that dynamic webpages are a bit more complicated to monitor by this way. You should remove dynamic element from the webpage before computing the checksum.  

First, enter the list of URL to scan in `url_list.txt`.

To initialize the working directory `workdir`, you will need to launch:

```bash
check_urls.sh init
```

Then, you can do a basic scan:

```bash
check_urls.sh check
```

You will find your results in `json` format in your `OUTPUT_DIR` defined in `profile.conf`.

If there is no output on stdout, that is normal. It means that nothing changed since last scan.

To display more informations about differences:

```bash
# compare from last run
check_urls.sh compare
# or check differences since the init step
check_urls.sh compare2init
```

Finally, you can add a cron job to produce results every N minutes/days...

If you detect any changes (see Json files in directory `OUTPUT_DIR`), then, you can also compare it manually:

```bash
cd /usr/local/websitechecks/var
cp -r workdir/ `date +"%Y%m%d"`_workdir
diff -rq <previous_workdir> `date +"%Y%m%d"`_workdir
# then compare specific html pages
diff -Ebw <previous_workdir>/website.index.html `date +"%Y%m%d"`_workdir/website.index.html

# finally, if everything is ok, regenerate a workdir
check_urls.sh clean
check_urls.sh init
```

After, you will have many directories with all your webpages histories.

## Uninstall

```bash
sudo make clean
```

## More useful tools

Check broken links :
  - http://home.snafu.de/tilman/xenulink.html (need `wine` on Linux)

Other systems to check HTTPS websites :
  - mixed content : 
    - https://mixed.octopuce.fr

Checking headers :
  - https://securityheaders.com
  - https://observatory.mozilla.org

Tool scan list :
  - https://www.owasp.org/index.php/Category:Vulnerability_Scanning_Tools
  - https://sectools.org/tag/web-scanners/
