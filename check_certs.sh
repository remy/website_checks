#!/bin/bash

CURDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DATE=`date '+%Y%m%d_%H%M%S'`


if [ -f /etc/default/websitechecks ]; then
  . /etc/default//websitechecks
  if [ -z "${OUTPUT_DIR}" ]; then
    OUTPUT_DIR="${VARDIR}/results"
  fi
  if [ -f "${LIBEXECDIR}/ssl-cert-info.sh" ]; then
    CERT_INFO="${LIBEXECDIR}/ssl-cert-info.sh"
  fi
  if [ -f "${ETCDIR}/host_https_list.txt" ]; then
    HOST_LIST="${ETCDIR}/host_https_list.txt"
  fi
else
  . ./profile.conf
  cd ${DIR}
  if [ -z "${OUTPUT_DIR}" ]; then
    OUTPUT_DIR="./results"
  fi
  CERT_INFO="${DIR}/ssl-cert-info.sh"
  HOST_LIST=host_https_list.txt
fi

if [ ! -d ${OUTPUT_DIR} ]; then
  mkdir -p ${OUTPUT_DIR}
fi

websites=`awk '{ if ($1 !~ "^#") {print;} }'  ${HOST_LIST}`
TOTAL_WEBSITES=`echo ${websites}|awk '{print NF}'`
OUTPUT="${OUTPUT_DIR}/${DATE}_check_certs.json"

#formatting output for jsonreader
i=1
results="{"
for host in ${websites}
do
    results="${results}\n\t\"${host}\":{"
    results="${results}\n\t\t\"enddate\":\t\""
    END_DATE=`bash ${CERT_INFO} --host $host --end | sed "s/$/\",/"`
    results="${results}${END_DATE}"
    results="${results}\n\t\t\"status\":\t\""
    END_OK=`bash ${CERT_INFO} --host $host --end-check|sed "s/$/\"/"`
    results="${results}${END_OK}"
    if [ $i -eq $TOTAL_WEBSITES ]; then
        results="${results}\n\t}"
    else
        results="${results}\n\t},"
    fi
    i=$((i+1))
done
results="${results}\n}"

echo -e ${results} | tee ${OUTPUT}

ssl_answers=`for host in ${websites}; do echo -n "$host:" && bash ${CERT_INFO} --host $host --end-check; done`
count=`echo "${ssl_answers}" | grep -c "Ok"`

if [ $count -ne $TOTAL_WEBSITES ]; then 
  echo "${ssl_answers}" |mail -s "${MAIL_SUBJECT}" $MAIL
fi

cd ${CURDIR}
