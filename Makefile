all: install

install: certs urls

dirs:
	mkdir -p /usr/local/websitechecks/etc
	mkdir -p /usr/local/websitechecks/var/workdir
	mkdir /usr/local/websitechecks/var/results
	mkdir /usr/local/websitechecks/libexec
	chmod -R 777 /usr/local/websitechecks/var

status_conf: status

certs_conf: status

status: dirs
	cp profile.conf /etc/default/websitechecks
	cp host_https_list.txt /usr/local/websitechecks/etc/

certs: status
	cp check_certs.sh /usr/local/bin
	cp ssl-cert-info.sh /usr/local/websitechecks/libexec
	chmod +x /usr/local/bin/check_certs.sh

url_conf: dirs
	cp url_list.txt /usr/local/websitechecks/etc/

urls: url_conf
	cp check_urls.sh /usr/local/bin
	cp check_sha256.awk /usr/local/websitechecks/libexec
	chmod +x /usr/local/bin/check_urls.sh

clean:
	rm -rf /usr/local/websitechecks
	rm -f /usr/local/bin/check_urls.sh
	rm -f /usr/local/bin/check_certs.sh
	rm -f /etc/default/websitechecks
