#!/usr/bin/env awk

#BEGIN {
#  print "\t{";
#  FS="[/:]";
#}

#FNR => compteur par fichier
{
  longname = sitename".index.html";
  if ($2 == longname) {
    if ( NR == FNR ) { sha256=$1; }
    else {
      # total is the total of line for all sha256sum.txt* files
      if (NR==total) {
        if ($1 != sha256) {print "\t\""sitename" - "url"\":\t\"NOT OK\"";}
        else {print "\t\""sitename" - "url"\":\t\"OK\"";}
      }
      else {
        if ($1 != sha256) {print "\t\""sitename" - "url"\":\t\"NOT OK\",";}
        else {print "\t\""sitename" - "url"\":\t\"OK\",";}
      }
    }
  }
}

#END {
#  if (FNR==total) { print "\t}";}
#  else { print "\t},";}
#}
